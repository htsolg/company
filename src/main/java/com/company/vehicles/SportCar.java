package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public class SportCar extends Car{

    private int maxSpeed;

    public SportCar(String brand, String model, float weight, Driver driver, Engine engine, int maxSpeed) {
        super(brand, model, weight, driver, engine);
        this.maxSpeed = maxSpeed;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public void start() {
        System.out.println("Вирушаю на змагання");
    };
    public void stop() {
        System.out.println("Прибув на місце змагань");
    };
    public void turnRight() {
        System.out.println("Повертаю праворуч без зменшення швидкості");
    };
    public void turnLeft() {
        System.out.println("Повертаю ліворуч з прискоренням");
    };


    @Override
    public String toString() {
        return "SportCar{" +
                "maxSpeed=" + maxSpeed +
                ", weight=" + getWeight() +
                ", driver=" + getDriver() +
                ", engine=" + getEngine() +
                "} " + super.toString();
    }
}
