package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public class Lorry extends Car{

    private float carryingCapacity;

    public Lorry(String brand, String model, float weight, Driver driver, Engine engine, float carryingCapacity) {
        super(brand, model, weight, driver, engine);
        this.carryingCapacity = carryingCapacity;
    }

    public float getCarryingCapacity() {
        return carryingCapacity;
    }

    public void setCarryingCapacity(float carryingCapacity) {
        this.carryingCapacity = carryingCapacity;
    }


    public void start() {
        System.out.println("Починаю тягнути вантаж");
    };
    public void stop() {
        System.out.println("Прибув на місце розвантаження");
    };
    public void turnRight() {
        System.out.println("Повертаю направо з вантажем");
    };
    public void turnLeft() {
        System.out.println("Повертаю наліво з вантажем");
    };


    @Override
    public String toString() {
        return "Lorry{" +
                "carryingCapacity=" + carryingCapacity +
                ", weight=" + getWeight() +
                ", driver=" + getDriver() +
                ", engine=" + getEngine() +
                "} " + super.toString();
    }
}
