package com.company;

import com.company.details.Engine;
import com.company.professions.Driver;
import com.company.vehicles.Car;
import com.company.vehicles.Lorry;
import com.company.vehicles.SportCar;

public class Main {
    public static void main(String[] args) {

        Driver driver = new Driver("Van Dam", 34, "male", "888-307-665",12);
        Engine engine = new Engine(176, "Germany");
        Car car = new Car("WV", "Tiguan", 1699, driver, engine);
        Lorry lorry = new Lorry("MAN", "TGS", 4851, driver, engine, 28510);
        SportCar sportCar = new SportCar(" Mazda", " MX-5", 1399, driver, engine, 320);



        System.out.println(car);
        car.start();
        lorry.start();
        lorry.turnRight();
        sportCar.turnLeft();
        sportCar.turnRight();
        sportCar.setMaxSpeed(320);
        sportCar.start();
        sportCar.stop();
    }
}